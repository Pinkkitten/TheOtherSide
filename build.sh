#!/usr/bin/env bash
set -x

# Set those lines to fit your setup.
# This is where TheOtherSide.qsp will be copied. If you don't want to move it just comment (#) the line below.
#DESTDIR=../GL_ECV

# The file that will be generated or open
QSPFILE=TheOtherSide.qsp

#######################################################################

./txtmerge.py locations TheOtherSide.txt
if [[ "$OSTYPE" == "linux-gnu" ]]; then
	./txt2gam.linux TheOtherSide.txt "${QSPFILE}" 1> /dev/null
elif [[ "$OSTYPE" == "darwin"* ]]; then
	./txt2gam.mac TheOtherSide.txt "${QSPFILE}" 1> /dev/null
elif [[ "$OSTYPE" == "msys" ]]; then
	if [[ "$MSYSTEM_CARCH" == "x86_64" ]]; then
		./txt2gam64.exe TheOtherSide.txt "${QSPFILE}" 1> /dev/null
	else
		./txt2gam.exe TheOtherSide.txt "${QSPFILE}" 1> /dev/null
	fi
fi
if [ -d "${DESTDIR}" ]; then
	cp --reflink=auto "${QSPFILE}" "${DESTDIR}"
fi

